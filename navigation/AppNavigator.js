import React from 'react';
import {View, StyleSheet, ActivityIndicator, StatusBar, AsyncStorage} from 'react-native'
import {createStackNavigator, createSwitchNavigator, createAppContainer} from 'react-navigation';

import Weather from '../screens/Weather';
import WeatherCard from "../screens/WeatherCard";

class AuthLoadingScreen extends React.Component {
    constructor() {
        super();
        this.props.navigation.navigate('Weather_');
    }

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator/>
                <StatusBar barStyle="default"/>
            </View>
        );
    }

}

const WeatherScreen = createStackNavigator({
    Weather: Weather,
    WeatherCard: WeatherCard
});

export default createAppContainer(createSwitchNavigator(
    {
        Weather_: WeatherScreen 
},
{
    initialRouteName: 'Weather_'
}
));

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
