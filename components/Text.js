import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

const Advice = ({item}) => {
    if (parseInt(item) >= 200 && parseInt(item) < 300)
    return (
        <Text style={styles.notes}>Stay at home, plz!</Text>
    );
    if (parseInt(item) >= 300 && parseInt(item) < 500)
        return (
            <Text style={styles.notes}>Little rain in ur life...take an umbrella.</Text>
        );
    if (parseInt(item) >= 500 && parseInt(item) <600)
        return (
            <Text style={styles.notes}>U really need to take an umbrella!</Text>
        );
    if (parseInt(item) >= 600 && parseInt(item) < 700)
        return (
            <Text style={styles.notes}>Dress warmer.</Text>
        );
    if (parseInt(item) >= 700 && parseInt(item) < 800)
        return (
            <Text style={styles.notes}>Take sunglasses... It will be strange air</Text>
        );
    if (parseInt(item) === 800)
        return (
            <Text style={styles.notes}>It will be great day!</Text>
        );
    if (parseInt(item) > 800)
        return (
            <Text style={styles.notes}>U can go to play basketball.</Text>
        )
};

const styles = StyleSheet.create({
    notes: {
        fontSize: 18,
        color:'#fff',
        textTransform:'capitalize',
        textAlign: 'center'
    }
});

export default Advice;