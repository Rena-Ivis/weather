import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import {Card, Divider } from 'react-native-elements';
import ImageWeather from "./ImageWeather";

    function getTime(item) {
    let date = new Date(item.dt * 1000);
    let hours = date.getHours();
    let minutes = "0" + date.getMinutes();
   return ((((hours - 4)<0)?hours+20:hours - 4) + ':' + minutes.substr(-2));
}

const item = ({item}) => {
    return (
        <Card containerStyle={styles.card}>
            <Text style={styles.notes}>{item.dt_txt.slice(0,10)}</Text>
            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-around'}}>
                <ImageWeather item={item}/>
                <Text style={styles.time}>{getTime(item)}</Text>
            </View>

            <Divider style={{ backgroundColor: '#dfe6e9', marginVertical:1}} />

            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                <Text style={styles.notes}>{item.weather[0].description} </Text>
                <Text style={styles.notes}>{Math.round( item.main.temp-273.15) }&#8451;</Text>
            </View>
        </Card>
    )
};

const styles = StyleSheet.create({
    card:{
        backgroundColor:'#616161',
        borderWidth:0,
        borderRadius:20,
        height: 140,
        width: 200
    },
    time:{
        fontSize:30,
        color:'#fff'
    },
    notes: {
        fontSize: 16,
        color:'#fff',
        textTransform:'capitalize'
    }
});

export default item;