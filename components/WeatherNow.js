import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {Card, Divider} from 'react-native-elements';
import ImageWeather from "./ImageWeather";
import Advice from './Text'

const item = ({item}) => {
    return (
        <View>
        <Card containerStyle={styles.card}>
            <View style={{flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center'}}>
                <ImageWeather item={item}/>
                <Text style={styles.time}>Now</Text>
            </View>

            <Divider style={{backgroundColor: '#dfe6e9', marginVertical: 20}}/>

            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={styles.notes}>{item.weather[0].description}</Text>
                <Text style={styles.notes}>{Math.round(item.main.temp - 273.15)}&#8451;</Text>
            </View>
        </Card>
            <Card containerStyle={styles.card}>
                <Advice item={item.weather[0].id}/>
            </Card>
        </View>
)
};

const styles = StyleSheet.create({
    card:{
    backgroundColor:'#616161',
    borderWidth:0,
    borderRadius:20,
    },
    time:{
    fontSize:38,
    color:'#fff'
},
    notes: {
    fontSize: 18,
    color:'#fff',
    textTransform:'capitalize'
}
});

export default item;