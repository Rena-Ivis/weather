import React from 'react';
import Sun from '../Images/01d'
import Moon from '../Images/01n'
import Clouds from '../Images/02d'
import CloudsN from '../Images/02n'
import Clouds2 from '../Images/03dn'
import Clouds3 from '../Images/04d'
import Clouds3n from '../Images/04n'
import Rain2 from '../Images/09dn'
import Rain from '../Images/10dn'
import Storm from '../Images/11dn'
import Snow from '../Images/13dn'
import Mist from '../Images/50dn'

const ImageWeather = ({item}) => {
    if(item.weather[0].icon==='01d')
        return (<Sun/>);
    if(item.weather[0].icon==='01n')
        return (<Moon/>);
    if(item.weather[0].icon==='02d')
        return (<Clouds/>);
    if(item.weather[0].icon==='02n')
        return (<CloudsN/>);
    if(item.weather[0].icon==='03d' || item.weather[0].icon==='03n')
        return (<Clouds2/>);
    if(item.weather[0].icon==='04d')
        return (<Clouds3/>);
    if(item.weather[0].icon==='04n')
        return (<Clouds3n/>);
    if(item.weather[0].icon==='09d' || item.weather[0].icon==='09n')
        return (<Rain2/>);
    if(item.weather[0].icon==='10d'|| item.weather[0].icon==='10n')
        return (<Rain/>);
    if(item.weather[0].icon==='11d' || item.weather[0].icon==='11n')
        return (<Storm/>);
    if(item.weather[0].icon==='13d'|| item.weather[0].icon==='13n')
        return (<Snow/>);
    if(item.weather[0].icon==='50d'|| item.weather[0].icon==='50n')
        return (<Mist/>)
};
export default ImageWeather;