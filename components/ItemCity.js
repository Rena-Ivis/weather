import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';


const styles = StyleSheet.create({
    container: {
        height: 50,
        textAlign: "center",
        marginTop: 4
    },
    text: {
        fontSize: 16,
        color: '#000'
    }
});

const item = ({text}) => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>{text}</Text>
        </View>
    )
};
export default item;