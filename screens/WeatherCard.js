import React from 'react';
import {
    StyleSheet,
    Alert,
    Text,
    View,
    TouchableHighlight,
    FlatList,
    Dimensions,
    AsyncStorage,
    ScrollView,
    RefreshControl
} from 'react-native';

import Axios from 'axios';
import Item from '../components/Item';
import ItemNow from '../components/WeatherNow';

const {width} = Dimensions.get('window');

export default class WeatherCard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            searchCity: '',
            data: [],
            weatherNow: [],
            textHelp: '',
            error: false,
            searchButton: 1,
            weather: [],
            refreshing: true
        };
        this._onRefresh();
    }

    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.getParam('Title', 'City Weather'),
            headerStyle: {
                backgroundColor: navigation.getParam('BackgroundColor', '#212121'),
            },
            headerTintColor: navigation.getParam('HeaderTintColor', '#fff'),
        };
    };

    async getWeatherNow() {
        let city = await AsyncStorage.getItem('city');
        let now = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=2493856c1a6028ed66c218059750333a`;
        Axios
            .get(now)
            .then(response => {
                this.setState({weatherNow: response.data});
            })
            .catch(error => {
                console.log(error);
            });
    }

    async getWeatherData() {
        let city = await AsyncStorage.getItem('city');
        let url = `http://api.openweathermap.org/data/2.5/forecast?q=${city}&?mode=json&APPID=2493856c1a6028ed66c218059750333a`;
        await Axios
            .get(url)
            .then(response => {
                let weather = response.data.list;
                this.setState({weather: weather, textHelp: response.data.city.name});
            })
            .catch(error => {
                console.log(error);
            });
    };

    getOneDay() {
        let {data, weather} = this.state;
        data = weather.slice(0, 8);
        this.setState({data});
    };

    getThreeDays() {
        let {data, weather} = this.state;
        data = weather.slice(0, 24);
        this.setState({data});
    }

    getFiveDays() {
        let {data, weather} = this.state;
        data = weather.slice(0, 40);
        this.setState({data});
    }

    ChangeCity() {
        this.props.navigation.navigate('Weather');
    }

    _onRefresh() {
        this.setState({refreshing: true});
        this.getWeatherData().then(() => {
            this.getWeatherNow();
            let {searchButton} = this.state;
            searchButton === 1 ? this.getOneDay() : (searchButton === 2 ? this.getThreeDays() : this.getFiveDays())
        }).then(()=>{
            this.setState({refreshing: false});
        });
    };

    render() {
        const {data, textHelp, weatherNow, searchButton} = this.state;
        return (
            <View style={styles.container}>
                <View style={{flexDirection: "row"}}>
                    <TouchableHighlight
                        style={[styles.ButtonStyle, (searchButton === 1 ? {backgroundColor: "#212121"} : styles.ButtonStyle)]}
                        onPress={() => {
                            this.getOneDay();
                            this.setState({searchButton: 1})
                        }}
                    ><Text style={styles.TextColor}>Day</Text></TouchableHighlight>
                    <TouchableHighlight
                        style={[styles.ButtonStyle, (searchButton === 2 ? {backgroundColor: "#212121"} : styles.ButtonStyle)]}
                        onPress={() => {
                            this.getThreeDays();
                            this.setState({searchButton: 2})
                        }}
                    ><Text style={styles.TextColor}>3 days</Text></TouchableHighlight>
                    <TouchableHighlight
                        style={[styles.ButtonStyle, (searchButton === 3 ? {backgroundColor: "#212121"} : styles.ButtonStyle)]}
                        onPress={() => {
                            this.getFiveDays();
                            this.setState({searchButton: 3})
                        }}
                    ><Text style={styles.TextColor}>5 days</Text></TouchableHighlight>
                </View>

                <View>
                    <ScrollView
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh.bind(this)}
                            />
                        }
                    >
                        <View>
                            <Text style={styles.notes}>{textHelp}</Text>

                            {weatherNow.length !== 0 ? <ItemNow
                                item={weatherNow}
                            /> : null}

                            <FlatList
                                showsHorizontalScrollIndicator={false}
                                horizontal
                                extraData={this.state}
                                data={data}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({item}) =>
                                    <Item
                                        item={item}
                                    />
                                }
                            />
                        </View>
                    </ScrollView>
                </View>

                <TouchableHighlight
                    style={styles.btn}
                    onPress={() => {
                        this.ChangeCity()
                    }}
                ><Text style={styles.TextColor}>Change city</Text></TouchableHighlight>
            </View>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#212121',
    },
    btn: {
        position: 'absolute',
        width: width,
        height: 50,
        backgroundColor: '#3F51B5',
        justifyContent: 'center',
        borderTopEndRadius: 5,
        borderTopStartRadius: 5,
        bottom: 0
    },
    ButtonStyle: {
        width: width - (width / 3 * 2),
        height: width - (width / 8 * 7),
        borderWidth: 1,
        borderBottomStartRadius: 5,
        borderBottomEndRadius: 5,
        backgroundColor: '#515151',
        justifyContent: 'center'
    },
    TextColor: {
        color: '#fff',
        textAlign: 'center',
    },
    notes: {
        fontSize: 38,
        color: '#fff',
        textTransform: 'capitalize',
        textAlign: 'center',
    }
});
