import React from 'react';
import {
    StyleSheet,
    Alert,
    Text,
    View,
    TouchableHighlight,
    FlatList,
    TextInput,
    Dimensions,
    AsyncStorage,
    TouchableOpacity
} from 'react-native';

import {SearchBar} from 'react-native-elements';
import {SwipeRow, SwipeListView} from 'react-native-swipe-list-view'

const {width} = Dimensions.get('window');

export default class Weather extends React.Component {

    state = {
        data: [],
        cities: [{city: 'Moscow'}, {city: 'Saint Petersburg'}, {city: 'Ulyanovsk'}],
        show: false,
        textSearch: '',
        error: false,
        arrayHolder: [],
        visible: false,
        selectedRow: false
    };

    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.getParam('Title', 'Select city'),
            headerStyle: {
                backgroundColor: navigation.getParam('BackgroundColor', '#212121'),
            },
            headerTintColor: navigation.getParam('HeaderTintColor', '#fff'),
        };
    };

    async getCities() {
        let cities = await AsyncStorage.getItem('saveCity');
        cities = JSON.parse(cities);
        cities.map((value, index)=>{
            value.key = index.toString();
            cities[index] = value;
        });
        this.setState({cities});
    }

    async componentDidMount() {
        await this.getCities();
        this.getCityList();
    }

    getCityList() {
        const url = '../components/city.list.min.json';
        let rec = require(url);
        this.setState({data: rec}, () => {
            console.log(this.state.data)
        });
        this.setState({arrayHolder: rec});
    }

    searchFilterFunction = async text => {
        this.state.arrayHolder = this.state.data;
        const newData = this.state.arrayHolder.filter(item => {
            const itemData = `${item.name.toUpperCase()}`;
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({arrayHolder: newData});
        await this.setState({textSearch: text});
        if (this.state.textSearch === '')
            this.setState({show: false});
        else
            this.setState({show: true})
    };

    async GetItem(item) {
        let {cities} = this.state;
        await AsyncStorage.setItem('city', item);
        this.setState({textSearch: item});
        let found = '';
        cities.find(function (element) {
            if (element.city === item) {
                found = item;
            }
        });
        if (found !== item) {
            cities.push({city: item});
            this.setState({cities});
            cities.map((value, index)=>{
                value.key = index.toString();
                cities[index] = value;
            });
            let information = JSON.stringify(cities);
            await AsyncStorage.setItem('saveCity', information);
        }
        this.props.navigation.navigate('WeatherCard');
    }

    async deleteItem(key) {
        let {cities} = this.state;
        cities.splice(key, 1);
        cities.map((value, index)=>{
            value.key = index.toString();
            cities[index] = value;
        });
        this.setState({cities: cities});
        let information = JSON.stringify(this.state.cities);
        await AsyncStorage.setItem('saveCity', information);
        this.setState({selectedRow: true})
    }

    async removeRequest(rowMap, key) {

        if (rowMap[key]) {
            await rowMap[key].closeRow();
        }
        Alert.alert(
            'Delete',
            'Do u want to delete?',
            [
                {text: 'Cancel',
                    onPress: () => {},
                    // style: 'cancel',
                },
                {text: 'Yes', onPress: async () => {
                        await this.deleteItem(key)
                    }},
            ],
            {cancelable: false},
        );
    }

    render() {
        const {textSearch, arrayHolder, cities} = this.state;
        return (
            <View style={styles.container}>
                <View style={styles.container}>
                    <SearchBar
                        round
                        searchIcon={{size: 24}}
                        containerStyle={{backgroundColor: '#353535'}}
                        inputStyle={{color: '#fff'}}
                        inputContainerStyle={{borderRadius: 5}}
                        onChangeText={text => this.searchFilterFunction(text)}
                        onClear={text => this.searchFilterFunction('')}
                        placeholder="Type Here..."
                        value={textSearch}
                    />
                    {this.state.show ? (
                            <FlatList style={{height: '82.5%'}}
                                      extraData={this.state}
                                      data={arrayHolder}
                                      renderItem={({item}) => (
                                          <Text style={styles.Item}
                                                onPress={() => {
                                                    this.GetItem(item.name)
                                                }}>
                                              {item.name}
                                          </Text>
                                      )}
                                      enableEmptySections={true}
                                      keyExtractor={(item, index) => index.toString()}
                            />
                        ) :
                        <SwipeListView
                            data={this.state.cities}
                            renderItem={ (data, rowMap) => (
                                <SwipeRow rightOpenValue={-100}>
                                    <View style={styles.hidden}>
                                        <TouchableOpacity onPress={ _ => this.removeRequest(rowMap, data.item.key) }>
                                            <Text style={styles.TextColor}>Delete</Text>
                                        </TouchableOpacity>
                                    </View>
                                        <View style={styles.Item}>
                                            <Text style={styles.TextColor} onPress={() => {
                                                this.GetItem(data.item.city)
                                            }}>{data.item.city}</Text>
                                        </View>
                                </SwipeRow>
                            )}
                        />}
                </View>
                <TouchableHighlight
                    style={styles.btn}
                    onPress={async () => {
                        await this.GetItem(textSearch)
                    }}
                ><Text style={styles.TextColor}>Weather</Text></TouchableHighlight>
            </View>
        );
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#353535'
    },
    btn: {
        width: width,
        height: 50,
        backgroundColor: '#3F51B5',
        justifyContent: 'center',
        borderTopEndRadius: 5,
        borderTopStartRadius: 5,
        bottom: 0,
        position: 'absolute',
    },
    TextColor: {
        color: '#fff',
        textAlign: 'center',
        justifyContent: 'center'
    },
    Item: {
        backgroundColor: '#616161',
        height: 30,
        margin: 4,
        color: '#fff',
        textAlign: 'center',
        textAlignVertical: 'center',
        borderRadius: 5,
    },
    hidden: {
        position: 'absolute',
        right: 0,
        height: 30,
        margin: 4,
        width: 100,
        backgroundColor: '#CF6679',
        justifyContent: 'center',
        alignItems: 'flex-end',
        borderRadius: 5,
        paddingRight: 30,
    }
});

